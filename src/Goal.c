
#include "common.h"
#include "main.c"

static SDL_Texture* flag[2];
static void touch(Entity *other);
int val;

void initGoal(char *line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;
	val = 1;
	e->texture = loadTexture("gfx/Flag.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->touch = touch;
}


static void touch(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		self->health = 0;

		other->score += (app.timer < 30)*100 + (app.timer < 60) * 50 + 10 + other->shields*10;

		char buffer[42] = { 0 };
		FILE* file;
		file = fopen("data/playerInfo.txt", "w");
		sprintf(buffer,"%d\n%d\n%d\n%d\n%d", other->health, other->shields, other->maxJumps, other->score, other->guns);
		fputs(buffer,file);
		fclose(file);

		/*
		FILE* lRead; int val;
		lRead = fopen("data/playerInfo.dat", "r");
		fgets(val, 1, lRead);
		fclose(lRead);

		app.lvl = val++;

		FILE* lWrite;
		lWrite = fopen("data/playerInfo.dat", "w");
		fprintf(lWrite, "%d", app.lvl);
		fclose(lWrite);
		*/
		initStage();
	}
}



