#include "common.h"

static void touch(Entity* other);
static void tick(void);

void initLava(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 3;

	e->texture = loadTexture("gfx/Lava.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->tick = tick;
	e->touch = touch;
}


static void tick(void)
{
	self->value += 0.1;
}

static void touch(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		if (player->timer <= 0) { // lava ignores shields //
			player->health--;
			player->timer = 100;
		}
	}
}
