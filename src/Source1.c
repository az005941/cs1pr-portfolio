#include "common.h"

static void touch(Entity* other);
static void tick(void);

void initEnemy(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/Pete01.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tick;
	e->touch = touch;
}

static void tick(void)
{
	self->value += 0.1;

	self->y += cos(self->value);
}

static void touch(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		player->health--;
	}
}
