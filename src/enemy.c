#include "common.h"

static void touch(Entity* other);
static void tick(void);

void initEnemy(char* line)
{
	Entity* e;
	
	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/Pete01.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->tick = tick;
	e->touch = touch;
}

void Jump() {
	if (self->isOnGround) {
		self->dy = -20;
		self->riding = NULL;
	}
}

static void tick(void)
{
	self->value += 0.1;

	int pyX = self->x - player->x;
	int pyY = self->y - player->y;
	
	if (abs(pyX) < 100 && abs(pyY) < 120) { // enemy range x and y //
		// if player nearby //
		if (pyX > 0) {
			self->dx = -3;
		}
		else {
			self->dx = 3;
		}
		if (pyY < 0) {
			Jump();
		}

	}
	else {
		// if player not nearby //
		srand(time(NULL));
		int r = rand() % 6;
		if (r == 1) {
			Jump();
		}
		if (self->dy > 0) {
			self->dy -= 1;
		}
		if (self->dx == 0) {
			r = rand() % 2;
			if (r == 1) {
				self->dx = -3;
			}
			else if (r == 0) {
				self->dx = 3;
			}
		}
	}
}

static void touch(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		if (player->timer <= 0) {
			if (player->shields > 0) {
				player->shields--;
			}
			else {
				player->health--;
			}
			player->timer = 180;
		}
	}
}
