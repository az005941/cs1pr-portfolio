#include "common.h"

static void tick(void);
static void touch(Entity* other);
static int held = 0;
static SDL_Texture* gun[2];

void initGun(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;
	gun[0] = loadTexture("gfx/GunR.png");
	gun[1] = loadTexture("gfx/GunL.png");
	e->texture = gun[0];
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tick;
	e->touch = touch;
}

static void tick(void)
{
	self->value += 0.1;
	if (held) {
		self->x = player->x-player->facing*18;
		self->y = player->y+20;
		self->texture = gun[player->facing];
	}
	else {
		self->y += cos(self->value);
	}
}

static void touch(Entity* other)
{
	if (self->health > 0 && other == player && held == 0)
	{
		//self->health = 0;
		held = 1;
		// add gun to players inv
	}
}
