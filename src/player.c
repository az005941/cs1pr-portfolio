/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static SDL_Texture *pete[6];
static SDL_Texture *none;

int textureIndex = 0;
int score = 0;
int dif;
int ctoi = (int)('0');

void initPlayer(void)
{
	player = malloc(sizeof(Entity));
	memset(player, 0, sizeof(Entity));


	stage.entityTail->next = player;
	stage.entityTail = player;
	player->timer = 0;
	player->facing = 0;
	stage.gunCount = 0;

	player->x = player->sx;
	player->y = player->sy;

	player->health = 3;
	player->shields = 0;
	player->maxJumps = 0;
	player->score = 0;
	
	/* Reading file save data in to player */

	FILE* file; char data[5];
	file = fopen("data/playerInfo.txt", "r");
	fgets(data, 5, file);
	fclose(file);

	player->health = data[0] -ctoi;
	player->shields = data[1] -ctoi+ 38;
	player->maxJumps = data[2] -ctoi +47;
	player->score = data[3] -ctoi + 100;
	player->guns = data[4] -ctoi + 100;
	
	
	player->jumps = player->maxJumps;

	pete[0] = loadTexture("gfx/PeteR0.png");
	pete[1] = loadTexture("gfx/PeteR1.png");
	pete[2] = loadTexture("gfx/PeteR2.png");
	pete[3] = loadTexture("gfx/PeteL0.png");
	pete[4] = loadTexture("gfx/PeteL1.png");
	pete[5] = loadTexture("gfx/PeteL2.png");

	none = loadTexture("gfx/Null.png");
	

	player->texture = pete[textureIndex];

	SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);
}

void doPlayer(void)
{
	player->dx = 0;
	if (player->timer % 2 != 0 && player->timer > 0) {
		player->texture = none;
	}

	if (app.keyboard[SDL_SCANCODE_A])
	{
		player->dx = -(PLAYER_MOVE_SPEED - player->health);
		
		if (player->timer % 2 != 0 && player->timer > 0) {
		}
		else {
			if (textureIndex > 2) textureIndex = 0;
			player->texture = pete[textureIndex + 3];
			player->facing = 1;
		}
		textureIndex++;
	}

	else if (app.keyboard[SDL_SCANCODE_D])
	{
		player->dx = (PLAYER_MOVE_SPEED - player->health);

		if (player->timer % 2 != 0 && player->timer > 0) {
		}
		else {
			if (textureIndex > 2) textureIndex = 0;
			player->texture = pete[textureIndex];
			player->facing = 0;
			textureIndex++;
		}
	}
	else {
		textureIndex = 3 * player->facing;
		player->texture = pete[textureIndex];
	}
	
	if (app.jump) {
		if (player->isOnGround) {
			player->riding = NULL;

			player->dy = -PLAYER_JUMP_FORCE;

			player->jumps = player->maxJumps;

			playSound(SND_JUMP, CH_PLAYER);
		}
		else if (player->jumps >= 0) {
			player->jumps--;
			player->riding = NULL;

			player->dy = -PLAYER_JUMP_FORCE;

			playSound(SND_JUMP, CH_PLAYER);
		}
		app.keyboard[SDL_SCANCODE_W] = 0;
	}

	if (app.keyboard[SDL_SCANCODE_SPACE] && app.fire == 0) {
		app.fire = 1;
	}

	if (app.fire && stage.gunCount < 5) {
		app.fire = 0;
		stage.gunCount++;
		initProjectile(player->x, player->y);
	}

	if (app.keyboard[SDL_SCANCODE_RETURN] && player->dialog == 0) {
		player->dialog = 1;
		initStage();
	}


	if (app.keyboard[SDL_SCANCODE_ESCAPE])
	{
		player->x = player->y = 0;
		app.keyboard[SDL_SCANCODE_ESCAPE] = 0;
		exit(1);
	}
	if (player->dy > 0){
		player->dy -= 1;
	}

	if (player->timer > 0) {
		player->timer--;
	}

	
}

int GetScore() {
	return score;
}

void AddScore(int val) {
	score += val;
}

void AddGun() {
	stage.gunCount++;
}
void AddJump() {
	player->maxJumps++;
}
