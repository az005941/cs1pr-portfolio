#include "common.h"

static void touch(Entity* other);
static void tick(void);

static SDL_Texture* bulletSprites[1];

void initProjectile(int x, int y)
{
	bullet = malloc(sizeof(Entity));
	memset(bullet, 0, sizeof(Entity));
	stage.entityTail->next = bullet;
	stage.entityTail = bullet;

	bullet->health = 1;
	bullet->health = 1;

	bullet->x = player->x + 30*(1-player->facing*2);
	bullet->y = player->y;

	bulletSprites[0] = loadTexture("gfx/BulletR.png");
	bulletSprites[1] = loadTexture("gfx/BulletL.png");

	bullet->dy = 0;

	if (player->facing) {
		bullet->texture = bulletSprites[0];
		bullet->dx = -BULLET_SPEED;
	}
	else {
		bullet->texture = bulletSprites[1];
		bullet->dx = BULLET_SPEED;
	}
	bullet->tick = tick;
	bullet->touch = touch;
	bullet->flags = EF_WEIGHTLESS +EF_SOLID;

	stage.gunCount++;

	SDL_QueryTexture(bullet->texture, NULL, NULL, &bullet->w, &bullet->h);
}


static void tick(void)
{
	bullet->value += 1;
	//bullet->x += BULLET_SPEED;

	if ((bullet->x > SCREEN_WIDTH || bullet->value > 100) && bullet->health) {
		stage.gunCount--;
		bullet->health = 0;
	}
}

static void touch(Entity* other)
{
	if (bullet->health > 0)
	{
		other->health--;
		stage.gunCount--;
		bullet->health = 0;
	}
}
