/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void logic(void);
static void draw(void);
static void drawHud(void);
static SDL_Texture* bulletTexture;
static SDL_Texture* enemyTexture;

void initStage(void) // change this so that player takes parameters so there variables carry between levels
{
	app.fade = 255;
	app.fVal = 1;

	app.start = 0;
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&stage, 0, sizeof(Stage));

	stage.entityTail = &stage.entityHead;
	
	initEntities(atoi(readFile("data/levelInfo.dat")));


	initPlayer();

	initMap();
}
/*
void initStart(void) {
	app.start = 1;
	app.fVal = 1;
	app.fade = 0;
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&stage, 0, sizeof(Stage));
	stage.entityTail = &stage.entityHead;
	initEntities(0);

	initPlayer();

	player->x = 400;
	player->y = 400;

	initStartMap();

}
*/
static void logic(void)
{
	doPlayer();

	doEntities();

	doCamera();

	if (player->health <= 0) {
		player->health = 3;
		FILE* file;
		file = fopen("data/playerInfo.txt", "w");
		fprintf(file, "%d\n%d\n%d\n%d\n%d\n", player->health, player->shields, player->maxJumps, player->score, player->guns);
		fclose(file);
		initStage();
	}
}

static void draw(void)
{
	SDL_SetRenderDrawColor(app.renderer, 128, 192, 255, 255);
	SDL_RenderFillRect(app.renderer, NULL);

	drawMap();

	drawEntities();

	drawHud();
}

static void drawHud(void)
{
	SDL_Rect r;

	r.x = 0;
	r.y = 0;
	r.w = SCREEN_WIDTH;
	r.h = 35;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
	SDL_RenderFillRect(app.renderer, &r);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);
	if (app.start) {
		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, app.fade, app.fade, app.fade, TEXT_CENTER, "PRESS ENTER TO BEGIN");
		if (app.fade < 1 || app.fade > 254) {
			app.fVal *= -1;
			app.fade -= app.fVal;
		}
		app.fade -= app.fVal;
	}
	drawText(SCREEN_WIDTH - 5, 5, 255, 255, 255, TEXT_RIGHT, "LEVEL: %2d  TIME: %3d  SHIELD: %d  HEALTH: %d  GUNS: %2d  SCORE :%4d", app.lvl,app.timer/60,player->shields,player->health,stage.gunCount, player->score);
}
